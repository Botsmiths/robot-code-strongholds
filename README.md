# Welcome #

Welcome to the Botsmiths 2016 robot code repository. 

## Getting started ##
All documentation is contained on the [Wiki](https://bitbucket.org/Botsmiths/robot-code-strongholds/wiki/Home), so head over there to get started.

## Getting the code ##
The easiest way to access the code is by using the built in git support of the Eclipse IDE. To install the Eclipse IDE, you can either use the automated [Botsmiths Launcher](http://www.files.botsmiths.org/launcher/release/BotsmithsLauncherSetup.exe) which will also automatically deliver and update any new tools, or you may install Eclipse manually as described in the FRC [documentation](http://wpilib.screenstepslive.com/s/4485/m/13503/l/145002-installing-eclipse-c-java).
Once inside Eclipse you can import the project from this repository using the URL at the top right.  

[Downloading for the first time](https://www.youtube.com/watch?v=nqIKiqqNNVU)  
[Uploading your changes](https://www.youtube.com/watch?v=tC5UWjZaZPM)  
[Getting the latest code](https://youtu.be/b61fKHMVngs)

## Programming contacts ##
**Tanner Mickelson**  
Programming mentor/repository admin   
email: [supertanner@gmail.com](mailto:supertanner@gmail.com) or [tanner@botsmiths.org](mailto:tanner@botsmiths.org)  
phone: (425) 530-4644  

**Kyle Buzzelle**  
Programming mentor  
email: [k.buzz2524@gmail.com](mailto:k.buzz2524@gmail.com)