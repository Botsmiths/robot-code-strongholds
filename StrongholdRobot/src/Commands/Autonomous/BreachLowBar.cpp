#include "BreachLowBar.h"

#include "../DriveForward.h"
#include "../SetLauncherAngle.h"

BreachLowBar::BreachLowBar()
{
	AddSequential(new SetLauncherAngle(4.315, 1.5f));

	AddSequential(new DriveForward(2.5f, 0.75f));
	//AddSequential(new DriveForward(2.0f, 0.2f, 1.0f));//Turn around

	AddSequential(new SetLauncherAngle(4.889, 1.5f));
}
