#include "DefaultBreach.h"

#include "../DriveForward.h"

DefaultBreach::DefaultBreach()
{
	AddSequential(new DriveForward(1.0f, -1.0f));
	AddSequential(new DriveForward(0.5f, 0.5f));
	AddSequential(new DriveForward(1.0f, -1.0f));
}
