#include "SetLauncherAngle.h"
#include "../Robot.h"

SetLauncherAngle::SetLauncherAngle(float setpoint, float maximumTime)
	:Setpoint(setpoint),
	 maxTime(maximumTime)
{

}

void SetLauncherAngle::Initialize()
{
	if (maxTime != 0.0f)
	{
		SetTimeout(maxTime);
	}

	Robot::launcher->Enable();
	Robot::launcher->SetSetpoint(Setpoint);
}

void SetLauncherAngle::Execute(){}

bool SetLauncherAngle::IsFinished()
{
	if (maxTime != 0.0f && IsTimedOut())
	{
		return true;
	}

	return Robot::launcher->OnTarget();
}

void SetLauncherAngle::End(){}

void SetLauncherAngle::Interrupted(){}
