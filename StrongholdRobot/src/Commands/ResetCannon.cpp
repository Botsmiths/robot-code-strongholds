#include "ResetCannon.h"

ResetCannon::ResetCannon()
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
}

// Called just before this Command runs the first time
void ResetCannon::Initialize()
{
	Robot::launcher->SetLauncherHigh();
}

// Called repeatedly when this Command is scheduled to run
void ResetCannon::Execute()
{

}

// Make this return true when this Command no longer needs to run execute()
bool ResetCannon::IsFinished()
{
	return Robot::launcher->OnTarget();
}

// Called once after isFinished returns true
void ResetCannon::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void ResetCannon::Interrupted()
{

}
