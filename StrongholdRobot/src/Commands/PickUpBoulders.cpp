#include "PickUpBoulders.h"
#include "../Robot.h"

PickUpBoulders::PickUpBoulders()
{
	Requires(Robot::launcher.get());
}

void PickUpBoulders::Initialize()
{
	Robot::launcher->SetWheelSpeed(-1.0f);
	Robot::launcher->ResetEjector();
}

void PickUpBoulders::Execute()
{

	//Robot::launcher->SetLauncherAngle(0.0f);
	//TODO: Adjust tilt angle of launcher to be low enough for boulders to be sucked in.
}

bool PickUpBoulders::IsFinished()
{
	return false;//This command will be run constantly while the button is held, so this always returns false.
}

void PickUpBoulders::End()
{
	Robot::launcher->SetWheelSpeed(0.0f);
}

void PickUpBoulders::Interrupted()
{
	End();
}
