#ifndef DriveForward_H
#define DriveForward_H

#include "WPILib.h"
#include "../Robot.h"

class DriveForward: public Command
{
public:

	///@param ftime How long to drive forward for.
	///@param fspeed How fast to drive forward for.
	DriveForward(float ftime, float fspeed, float fturn = 0.0f);

	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();

private:

	std::shared_ptr<DriveTrain> driveTrain;

	float time;
	float speed;
	float turn;
};

#endif
