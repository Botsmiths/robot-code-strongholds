#ifndef LaunchBoulder_H
#define LaunchBoulder_H

#include "WPILib.h"

class LaunchBoulder: public Command
{
public:

	LaunchBoulder(float power = 1.0f);

	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();

private:

	float LaunchSpeed;
	bool EjectFlag;
};

#endif
