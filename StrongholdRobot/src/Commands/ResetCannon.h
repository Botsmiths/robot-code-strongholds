#ifndef ResetCannon_H
#define ResetCannon_H

#include "WPILib.h"
#include "..\Robot.h"

class ResetCannon: public Command
{
public:
	ResetCannon();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif
