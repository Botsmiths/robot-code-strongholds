#ifndef SetLauncherAngle_H
#define SetLauncherAngle_H

#include "WPILib.h"

class SetLauncherAngle: public Command
{
	float Setpoint;

	float maxTime;

public:
	SetLauncherAngle(float setpoint, float maximumTime=0.0f);
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif
