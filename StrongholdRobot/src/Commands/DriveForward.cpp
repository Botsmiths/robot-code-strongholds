#include "DriveForward.h"

DriveForward::DriveForward(float ftime, float fspeed, float fturn)
	:time(ftime),
	 speed(fspeed),
	 turn(fturn)
{
	driveTrain = Robot::driveTrain;

	Requires(driveTrain.get());
}

void DriveForward::Initialize()
{
	SetTimeout(time);

	//Useful timing functions:
	//SetTimeout(5.0); to set how long this command should run for
	//IsTimedOut(); to check if the command has timed out (can call from IsFinished to make the command end once the timeout is reached)
	//TimeSinceInitialized(); to get how long the command has been running for (in seconds)
}

void DriveForward::Execute()
{
	driveTrain->Drive(speed, turn);
}

bool DriveForward::IsFinished()
{
	return IsTimedOut();
}

void DriveForward::End()
{
	//TODO: Stop the robot driving.
	driveTrain->Stop();
}

void DriveForward::Interrupted()
{
	End();
}
