#ifndef FLOAT_LOOKUP_TABLE_H
#define FLOAT_LOOKUP_TABLE_H
#include <cmath>
#include <vector>

///@brief Implements a lookup table for floating point functions.
class FloatLookupTable
{
public:

	///@brief Holds an entry in the lookup table.
	struct Entry
	{
		float x;
		float y;
	};

	///@brief Constructs a lookup table from an entry array.
	///@param entries The array of lookup table entries, this MUST be sorted by x value in ascending order for correct results.
	FloatLookupTable(const std::vector<Entry>& entries)
		:m_entries(entries),
		m_minVal(entries[0].x),
		m_maxVal(entries[entries.size()-1].x)
	{}

	///@brief Looks up the y value of the function given the x value.
	///@param x The x value to calculate a y value for.
	///@return The y value for the specified x value. If the x value is out of the lookup table's range,
	///then it will return the nearest y value at either end.
	float Get(float x)
	{
		if (x <= m_minVal)
		{
			return m_entries[0].y;
		}
		else if (x >= m_maxVal)
		{
			return m_entries[m_entries.size() - 1].y;
		}

		unsigned int idx = 0;
		float oldRange = (m_maxVal - m_minVal);
		if (oldRange == 0)
		{
			idx = 0;
		}
		else
		{
			float newRange = (float)m_entries.size() - 1;
			idx = (unsigned int)(((x - m_minVal) * newRange) / oldRange);
		}

		if (m_entries[idx].x < x)
		{
			float alpha = x - m_entries[idx].x;
			return WeightedAverage(m_entries[idx].y, m_entries[idx + 1].y, alpha);
		}

		return m_entries[idx].y;
	}

	float operator () (float x)
	{
		return Get(x);
	}

private:

	static float WeightedAverage(float v0, float v1, float t)
	{
		return (1 - t)*v0 + t*v1;
	}

	std::vector<Entry> m_entries;

	float m_minVal;
	float m_maxVal;
};

#endif
