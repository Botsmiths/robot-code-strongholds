//

#ifndef DRIVETRAIN_H
#define DRIVETRAIN_H
#include "Commands/Subsystem.h"
#include "WPILib.h"

/**
 *
 *
 * @author ExampleAuthor
 */
class DriveTrain: public Subsystem
{
private:

	std::shared_ptr<SpeedController> leftWheels;
	std::shared_ptr<SpeedController> rightWheels;

public:

	DriveTrain();

	void InitDefaultCommand();

	///@brief Drives the by the specified amounts.
	///@param forward How fast to move the robot forward.
	///@param rotate How much to rotate the robot by.
	void Drive(float forward, float rotate);

	///@brief Stops all wheels from driving.
	void Stop();
};

#endif
